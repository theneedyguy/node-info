# OpenShift Node Info

This is a status page for your OpenShift Cluster. All it does is display the status of all of your OpenShift Nodes on a web page. It can be configured via simple environment variables.

## Website

![Image](./images/page.png)

## Configuration

### Prerequisites

Make sure you are logged into the console of the cluster as the cluster administrator. (oc adm)

1. You must create a Service Account in a namespace and add the cluster-admin role to it. (Replace the brackets with the actual values.)

```bash
# Create the user in a namespace
oc create serviceaccount <serviceaccount-name> -n <namespace-name>
# As cluster admin, add the cluster-admin role to the serviceaccount.
oc adm policy add-cluster-role-to-user cluster-admin system:serviceaccount:<namespace-name>:<serviceaccount-name>
```

2. Then find the name of the token you would like to use and extract the value of the secret:

```bash
# Execute this and look for 'Token:'. There you will find all the secret names of the service account.
oc describe serviceaccount <serviceaccount-name> -n <namespace-name>
# Now get the token by executing this command:
oc describe secret <serviceaccount-name>-token-*****
# The value of 'token:' is to be used as the TOKEN environment variable of the app.
```

The app must be configured via environment variables.

| Environment variable |   Purpose    |
|----------------------|--------------|
| REJECT_SELF_SIGNED   |  Rejects self-signed certificates: 0 = Allow, 1 = Reject. (Default: 1) |
| API_HOST             |  The base url of the OpenShift Console i.e. [https://console.example.com](https://console.example.com) |
| TOKEN                |  The service account token to make requests to the API |

