const express = require('express')
const pug = require('pug')
// Allow user to change self signed cert preference by default no self-signed certs are allowed
// Change this env variable to allow self-signed certificates
var request = require('request').defaults({ 'rejectUnauthorized': Boolean(Number(process.env.REJECT_SELF_SIGNED)) })
//console.log(no_self_signed)

// Initialize app and template engine
const app = express()
app.set('view engine', 'pug')
const port = process.env.PORT || 8080
const api_url = process.env.API_HOST
const token = process.env.TOKEN
const sender_id = process.env.SENDER_ID || "default_openshift_sender"
const event_id = process.env.EVENT_ID || "default_openshift_event"



function getStatusAPI(req, response){
	// Connect to api and get all node information
  var url = api_url + "/api/v1/nodes";
  request_data = request.get({
    url: url,
    json: true,
    headers: {'Authorization': `Bearer ${token}`}
  }, (err, res, data) => {
    if (err) {
      console.log('Error:', err);
      response.send("An error has occurred. Please check the logs for the message.")
    } else if (res.statusCode !== 200) {
      console.log('Status:', res.statusCode);
      response.send("The response code was not 200. Please make sure the OpenShift Api is setup correctly.")
    } else {
      // data is already parsed as JSON
      response.setHeader("Content-Type", "application/json");
      // Define the json structure
      json_data = {};
      // The key we want to push data into
      json_key = "states"
      // Assign the key to the struct as an array
      json_data[json_key] = [];

      // Loop through each openshift node
      data.items.forEach(function(elem) { 
      // Get status for each node
      status = elem.status
      // Define the component status array
      component_status = [];
      // Go through each condition and push it to the component_status array.
      status.conditions.forEach(function(condition){
        switch (condition.type) {
          case "PIDPressure":
            if (condition.status === "False") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "ok"
                }
              );
              break;
            } 
            else if (condition.status === "True"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "error"
                }
              );
              break;
            }
            else if (condition.status === "Unknown"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "warning"
                }
              );
              break;
            }
          case "DiskPressure":
            if (condition.status === "False") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "ok"
                }
              );
              break;
            } 
            else if (condition.status === "True"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "error"
                }
              );
              break;
            }
            else if (condition.status === "Unknown"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "warning"
                }
              );
              break;
            }
          case "MemoryPressure":
            if (condition.status === "False") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "ok"
                }
              );
              break;
            } 
            else if (condition.status === "True"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "error"
                }
              );
              break;
            }
            else if (condition.status === "Unknown"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "warning"
                }
              );
              break;
            }
          case "OutOfDisk":
            if (condition.status === "False") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "ok"
                }
              );
              break;
            } 
            else if (condition.status === "True"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "error"
                }
              );
              break;
            }
            else if (condition.status === "Unknown"){
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "warning"
                }
              );
              break;
            }
          case "Ready":
            if (condition.status === "False") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "error"
                }
              );
              break;
            }
            else if  (condition.status === "Unknown") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "warning"
                }
              );
              break;
            }
            else if (condition.status === "True") {
              component_status.push(
                {
                  "type" : condition.type,
                  "message": condition.message,
                  "status" : "ok"
                }
              );
              break;
            }                 
         default:
            break;
        }

      })
      json_data[json_key].push({
      "target_ci_name" : elem.metadata.name,
      "sender_id" : sender_id,
      "event_id" : event_id,
      "availability_metrics" : component_status
      });
    component_status = [];
    });
      response.header("Content-Type",'application/json');
      response.send(JSON.stringify(json_data, null, 4))
    }
 });
};


// Make a http request and render data in template
function getStatus(req, response){
  // Connect to api and get all node information
  var url = api_url + "/api/v1/nodes";
  request_data = request.get({
    url: url,
    json: true,
    headers: {'Authorization': `Bearer ${token}`}
  }, (err, res, data) => {
    if (err) {
      console.log('Error:', err);
      response.send("An error has occurred. Please check the logs for the message.")
    } else if (res.statusCode !== 200) {
      console.log('Status:', res.statusCode);
      response.send("The response code was not 200. Please make sure the OpenShift Api is setup correctly.")
    } else {
      // data is already parsed as JSON
      // We send the json to the template to parse it and display pretty
      response.render('index', {title : 'Node Info', nodes: data.items, message: "Node Info"})
    }
});
};

// Serve overview to user
// On each request we get the latest node info by making a request and parsing it to html
app.get('/', (req, res) => {
  getStatus(req, res);
})

app.get("/api/v1/nodes", (req, res) => {
  getStatusAPI(req, res);
})

// Start server
const server = app.listen(port, () => {
  console.log(`Node Info is listening on port ${port}!`)
})

// Graceful shutdown
process.on('SIGTERM', () => {
  server.close((err) => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
  process.exit(0)
  })
})
