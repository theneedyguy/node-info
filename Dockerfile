FROM node:7
WORKDIR /app

COPY package.json /app
RUN npm install

COPY index.js /app
COPY views /app/views

USER nobody
EXPOSE 8080
CMD node index.js

